package pyramid_course_assignments.section3;
/*
    PROBLEM STATEMENT
        Reverse an integer using arrays.

    Example
        reverseIntegerUsingArrays(486) => 684
        reverseIntegerUsingArrays(-123) => -321
*/

public class ReverseIntegerUsingArrays {

    public static int reverseIntegerUsingArrays(int myInt) {
        // ↓↓↓↓ your code goes here ↓↓↓↓
        return 0;
    }
}
