package pyramid_course_assignments.section1;

/*
    PROBLEM STATEMENT
        Complete the two methods below, increment and decrement. Use the increment operator and the
        decrement operator accordingly.

    Example
        increment() => 2
        decrement() => 0
*/

public class IncrementDecrement {

    public static int increment(){
        // ↓↓↓↓ your code goes here ↓↓↓
        return 0;
    }
    public static int decrement(){
        // ↓↓↓↓ your code goes here ↓↓↓↓
        return 0;
    }
}
