package pyramid_course_assignments.section1;

/*
    PROBLEM STATEMENT
        Rearrange the data types below in order to pass the test.
*/

public class PrimitiveDataTypes {
    // ↓↓↓↓ your code goes here ↓↓↓↓

    boolean smallInteger;
    short mediumInteger;
    float largeInteger;
    int smallFloat;
    long largeFloat;
    byte iHaveCharacter;
    char takeABite;
    String thisIsTrue;

}
