package pyramid_course_assignments.section7;

import java.util.ArrayList;

/*
    PROBLEM STATEMENT
        Use a set to remove the duplicates from an ArrayList.

    EXAMPLES:
        removeDuplicatesWithASet([1, 1]) => [1]
        removeDuplicatesWithASet([1]) => [1]
        removeDuplicatesWithASet([]) => []
        removeDuplicatesWithASet([1, 1, 1, 2, 2]) => [1, 2]
*/

public class RemoveDuplicatesWithASet {
    public static ArrayList<Integer> removeDuplicatesWithASet(ArrayList<Integer> nums) {
        // ↓↓↓↓ your code goes here ↓↓↓↓
        return new ArrayList<>();
    }
}
