package pyramid_course_assignments.section7;

import java.util.ArrayList;
import java.util.HashMap;


/*
    PROBLEM STATEMENT
        Count the amount of duplicates in a hashmap. This method should return an ArrayList
        filled with strings that should be in a format that would easily be printable.
        The exact format should appear as the return in the example below.

    EXAMPLE:
        countDuplicatesWithHashmap([1, 1, 2, 4, 4, 3, 4, 4]) => ["There are: " "2 1s\n" "1 2s\n" "4 4s\n" "1 3s\n"]
*/

public class CountDuplicatesWithHashmap {
    public static ArrayList<String> countDuplicatesWithHashmap(ArrayList<Long> nums) {
        // ↓↓↓↓ your code goes here ↓↓↓↓
        return new ArrayList<>();
    }
}
