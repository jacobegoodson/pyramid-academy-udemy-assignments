package pyramid_course_assignments.section2;

/*
    PROBLEM STATEMENT
        The method switchItUp takes an integer between 1 and 5. Using a switch statement, return a string
        equivalent for the integer input. If none of the inputs match return "none matched".

    Example
        switchItUp(4) => "four"
*/

public class SwitchStatements {

    public static String switchItUp(int n){
        // ↓↓↓↓ your code goes here ↓↓↓↓
        return "";
    }
}
