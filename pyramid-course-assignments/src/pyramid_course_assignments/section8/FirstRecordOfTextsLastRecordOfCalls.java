package pyramid_course_assignments.section8;

/*
    Problem:
        What is the first record of texts and what is the last record of calls? The method
        firstAndLastRecordOfTexts should return an array containing these two strings.
        "First record of texts, <incoming number> texts <answering number> at time <time>"
        "Last record of calls, <incoming number> calls <answering number> at time <time>, lasting <duration> seconds"
        The text in brackets is what you need to fill in.

    Example:
        firstAndLastRecordOfTexts() => [
            "First record of texts, 911 texts Mama Sue at time Inf",
            "Last record of calls, 411 calls 123456798 at time 0200, lasting 987654321 seconds"
        ]
*/

public class FirstRecordOfTextsLastRecordOfCalls {
    public static String[] firstRecordOfTextsLastRecordOfCalls() {
        // ↓↓↓↓ your code goes here ↓↓↓↓
        return new String[]{};
    }
}
