package pyramid_course_assignments.section8;

/*
    PROBLEM:
        The telephone company wants to identify numbers that might be involved in
        telephone marketing. Create a set of possible telemarketers:
        these are numbers that make outgoing calls but never send texts,
        receive texts or receive incoming calls.

        The return string for potentialTelemarketers should list the telemarketers in
        lexicographic order with no duplicates.

    EXAMPLE:
        outgoingCallsFromBangalore() => "98440 68457 (044)45416964 94497"
*/

public class PotentialTelemarketers {
    public static String potentialTelemarketers() {
        // ↓↓↓↓ your code goes here ↓↓↓↓
        return "";
    }
}
